import random
print("ROCK PAPER SCISSORS")
r=1
user={}
comp={}
win={}
ppoints=0
cpoints=0
winner=None
while r<=10:
    print("Round ",r)
    print("Enter your choice:")
    uchoice=input()
    user[r]=uchoice
    c=["rock","paper","scissors"]
    if uchoice.lower() not in c:
        print("Enter valid choice")   #else:print("You chose:",uchoice)
    cchoice=random.choice(c)
    comp[r]=cchoice   #print("Computer chose:",cchoice)
    if uchoice.lower()=="rock" and cchoice=="scissors": winner='Player'
    if uchoice.lower() == "rock" and cchoice == "paper": winner = 'Computer'
    if uchoice.lower() == "rock" and cchoice == "rock": winner ='Tie'
    if uchoice.lower() == "paper" and cchoice == "scissors": winner ="Computer"
    if uchoice.lower() == "paper" and cchoice == "rock": winner = "Player"
    if uchoice.lower()== "paper" and cchoice == "paper": winner = "Tie"
    if uchoice.lower()== "scissors" and cchoice == "scissors": winner = "Tie"
    if uchoice.lower() == "scissors" and cchoice == "paper": winner = "Player"
    if uchoice.lower() == "scissors" and cchoice == "rock": winner = "Computer"
    win[r]=winner
    if winner == "Tie": #print("Tie")
        ppoints += 5
        cpoints+=5
    elif winner=="Player": #print(winner,"won round",r)
        ppoints += 10
    else: #print(winner, "won round", r)
        cpoints += 10
    r+=1
print("\nComputer Score", cpoints)
print("Player Score", ppoints)
print("Enter the round for which you need the information:")
n=int(input())
print("Player choice=", user.get(n))
print("Computer choice=", comp.get(n))
if win.get(n)=="Tie": print("Round",n,"was a tie")
else:print(win.get(n),"won Round ",n)